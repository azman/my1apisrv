<?php
/**
 * Run.php
 * - task for running local bash script
 * - extends Easy class
**/
require_once dirname(__FILE__).'/../include/Easy.php';
class Run extends Easy {
	protected $_do_usr; // current user id
	function __construct($method,$params,$inputs) {
		$this->_do_usr = 0;
		// check debug mode earlier, to apply next step
		if (!isset($this->_debug_)) {
			$this->_debug_ = DEBUG_MODE;
		}
		$this->_valid_ = $this->_debug_;
		parent::__construct($method,$params,$inputs);
		// check user id
		if ($this->_doauth!=null)
			$this->_do_usr = intval($this->doauth['id']); // integer id
		// ALWAYS log request
		require_once dirname(__FILE__).'/../include/Log.php';
		$logr = new Log(get_class($this));
		$logr->log("{raw}".json_encode([ "from" => $_SERVER['REMOTE_ADDR'],
			"user" => $this->_do_usr, "method" => $this->_method,
			"params" => $this->_params, "inputs" => $this->_inputs ])."\n");
	}
	function run() {
		// look for script to run
		$that = array_shift($this->_params);
		if (empty($that))
			$this->throw_this("What?");
		switch ($this->_method) {
		case 'GET':
			$prev = getcwd();
			chdir(RUN_PATH);
			if (!file_exists($that))
				$this->throw_this("Cannot find script! ($that)");
			// single argument allowed
			$data = array_shift($this->_params);
			$cmdd = "bash $that $data"; // assume bash is always available!
			$test = shell_exec($cmdd);
			$result['what'] = $cmdd;
			$result['that'] = $test;
			chdir($prev);
			break;
		case 'POST': $this->throw_this("Invalid POST!"); break;
		case 'PUT': $this->throw_this("Invalid PUT!"); break;
		case 'DELETE': $this->throw_this("Invalid DELETE!"); break;
		default: $this->throw_this("Unknown Request!");
		}
		return $result;
	}
}
?>
