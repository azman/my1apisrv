<?php
/**
 * Users.php
 * - task handler for user-related stuffs
 * - extends Task class
 * - hashing done by clients - maybe add salt later?
**/
require_once dirname(__FILE__).'/../include/Task.php';
class Users extends Task {
	function __construct($method,$params,$inputs) {
		if (isset($this->_valid_))
			$this->_valid_ = VALID_DONE;
		parent::__construct($method,$params,$inputs);
	}
	protected function check_userflag($userflag,$default) {
		switch ($userflag) {
			case USER_ADM: case USER_MOD: case USER_USR: return $userflag;
			default: return $default;
		}
	}
	protected function user_create() {
		$test = new TTableUser;
		$this->table_create($test->tname(),$test->tdata(),$test->tmore());
	}
	protected function user_delete() { // THIS DELETES TABLE!!!
		$table = USER_UTABLE;
		if ($this->table_exists($table)) {
			$this->table_delete($table);
		}
	}
	protected function user_insert($data) {
		// validation done prior
		$list = [];
		$keys = [ 'name'=>PDO::PARAM_STR, 'pass'=>PDO::PARAM_STR,
			'flag'=>PDO::PARAM_INT,'nick'=>PDO::PARAM_STR,
			'fname'=>PDO::PARAM_STR,'email'=>PDO::PARAM_STR ];
		$pre1 = "INSERT INTO ".USER_UTABLE." (";
		$pre2 = "VALUES (";
		foreach ($keys as $key => $val) {
			if (isset($data[$key])) {
				$pre1 = $pre1."$key,";
				$pre2 = $pre2.":$key,";
				$list[$key] = $val;
			}
		}
		if (empty($list)) $this->throw_this("INSERT param missing!");
		$pre1 = substr($pre1,0,-1);
		$pre2 = substr($pre2,0,-1);
		$prep = $pre1.") ".$pre2.")";
		try {
			$stmt = $this->prepare($prep);
			foreach ($list as $key => $val)
				$stmt->bindParam(":".$key,$data[$key],intval($val));
			$exec = $stmt->execute();
			$user = intval($this->_dbmain->lastInsertId());
		} catch (PDOException $err) {
			$this->throw_this("INSERT error (".$err->getMessage().")");
		}
		if (!$exec) $this->throw_this("User insert failed!");
		$stmt->closeCursor();
		return $user;
	}
	protected function user_update($user,$data) {
		// validation done prior
		$list = [];
		$keys = [ 'pass'=>PDO::PARAM_STR,'flag'=>PDO::PARAM_INT,
			'nick'=>PDO::PARAM_STR,'fname'=>PDO::PARAM_STR,
			'email'=>PDO::PARAM_STR ];
		$prep = "UPDATE ".USER_UTABLE." SET ";
		foreach ($keys as $key => $val) {
			if (isset($data[$key])) {
				$prep = $prep."$key=:$key, ";
				$list[$key] = $val;
			}
		}
		if (empty($list)) $this->throw_this("UPDATE param missing!");
		$prep = substr($prep,0,-2);
		$prep = $prep." WHERE id=:user";
		try {
			$stmt = $this->prepare($prep);
			foreach ($list as $key => $val)
				$stmt->bindParam(':'.$key,$data[$key],$val);
			$stmt->bindParam(':user',$user,PDO::PARAM_INT);
			$exec = $stmt->execute();
		} catch (PDOException $err) {
			$this->throw_this("UPDATE error (".$err->getMessage().")");
		}
		return $exec;
	}
	protected function user_remove($user) {
		$prep = "DELETE FROM ".USER_UTABLE." WHERE id=?";
		try {
			$stmt = $this->prepare($prep);
			$stmt->bindValue(1,$user,PDO::PARAM_INT);
			$exec = $stmt->execute();
		} catch (PDOException $err) {
			$this->throw_this("DELETE error (".$err->getMessage().")");
		}
		return $exec;
	}
	protected function group_create() {
		$test = new TTableGroup;
		$this->table_create($test->tname(),$test->tdata(),$test->tmore());
		$test = new TTableGroupUsers;
		$this->table_create($test->tname(),$test->tdata(),$test->tmore());
	}
	protected function group_delete() {
		$table = USER_GTABLE;
		if ($this->table_exists($table)) $this->table_delete($table);
		$table = USER_LTABLE;
		if ($this->table_exists($table)) $this->table_delete($table);
	}
	protected function group_insert($data) {
		// validation done prior
		$list = [];
		$keys = [ 'name'=>PDO::PARAM_STR,
			'full'=>PDO::PARAM_STR, 'flag'=>PDO::PARAM_INT ];
		$pre1 = "INSERT INTO ".USER_GTABLE." (";
		$pre2 = "VALUES (";
		foreach ($keys as $key => $val) {
			if (isset($data[$key])) {
				$pre1 = $pre1."$key,";
				$pre2 = $pre2.":$key,";
				$list[$key] = $val;
			}
		}
		if (empty($list)) $this->throw_this("INSERT param missing!");
		$pre1 = substr($pre1,0,-1);
		$pre2 = substr($pre2,0,-1);
		$prep = $pre1.") ".$pre2.")";
		try {
			$stmt = $this->prepare($prep);
			foreach ($list as $key => $val)
				$stmt->bindParam(':'.$key,$data[$key],$val);
			$exec = $stmt->execute();
			$guid = intval($this->_dbmain->lastInsertId());
		} catch (PDOException $err) {
			$this->throw_this("INSERT error (".$err->getMessage().")");
		}
		$stmt->closeCursor();
		$done = [];
		$done['guid'] = $guid;
		if (!empty($data['user'])||$this->_doauth!==null) {
			if (empty($data['user'])) $user = $this->_doauth['id'];
			else $user = intval($data['user']);
			$flag = USER_ADM;
			$prep = "INSERT INTO ".USER_LTABLE." (uid,gid,flag)";
			$prep = $prep." VALUES (:user,:guid,:flag)";
			try {
				$stmt = $this->prepare($prep);
				$stmt->bindValue(':user',$user,PDO::PARAM_INT);
				$stmt->bindValue(':guid',$guid,PDO::PARAM_INT);
				$stmt->bindValue(':flag',$flag,PDO::PARAM_INT);
				$exec = $stmt->execute();
			} catch (PDOException $err) {
				$this->throw_this("INSERT error (".$err->getMessage().")");
			}
			$stmt->closeCursor();
			$done['user'] = $user;
		}
		return $done;
	}
	protected function group_update($guid,$full,$flag=0) {
		// validation done prior
		$prep = "UPDATE ".USER_GTABLE.
			" SET full=:full,flag=:flag WHERE id=:guid";
		try {
			$stmt = $this->prepare($prep);
			$stmt->bindParam(':full',$full,PDO::PARAM_STR);
			$stmt->bindParam(':flag',$flag,PDO::PARAM_STR);
			$stmt->bindParam(':guid',$guid,PDO::PARAM_INT);
			$exec = $stmt->execute();
		} catch (PDOException $err) {
			$this->throw_this("UPDATE error (".$err->getMessage().")");
		}
		return $exec;
	}
	protected function group_find($name) {
		$prep = "SELECT id FROM ".USER_GTABLE." WHERE name=:name";
		try {
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':name',$name,PDO::PARAM_STR);
			$stmt->execute();
			$item = $stmt->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $err) {
			$this->throw_this("CHKGRP error (".$err->getMessage().")");
		}
		if ($item==false) $item = 0;
		else $item = $item['id'];
		return $item;
	}
	protected function group_users_insert($uid,$gid,$flag=USER_NOT) {
		$prep = "INSERT INTO ".USER_LTABLE." (uid,gid,flag)";
		$prep = $prep." VALUES (:user,:guid,:flag)";
		try {
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':user',$user,PDO::PARAM_INT);
			$stmt->bindValue(':guid',$guid,PDO::PARAM_INT);
			$stmt->bindValue(':flag',$flag,PDO::PARAM_INT);
			$exec = $stmt->execute();
		} catch (PDOException $err) {
			$this->throw_this("INSERT error (".$err->getMessage().")");
		}
		$stmt->closeCursor();
		return $exec;
	}
	protected function group_users_update($id,$flag=USER_NOT) {
		$prep = "UPDATE ".USER_LTABLE." SET flag=:flag WHERE id=:id";
		try {
			$stmt = $this->prepare($prep);
			$stmt->bindParam(':flag',$flag,PDO::PARAM_INT);
			$stmt->bindParam(':id',$id,PDO::PARAM_INT);
			$exec = $stmt->execute();
		} catch (PDOException $err) {
			$this->throw_this("UPDATE error (".$err->getMessage().")");
		}
		return $exec;
	}
	protected function group_users_find($uid=0,$gid=0) {
		$pre0 = "SELECT T1.flag";
		// list group info
		$preG = ", T1.gid as guid, T2.name as gstr, T2.full as gtxt";
		// list user info
		$preU = ", T1.uid as uuid, T3.name as name, T3.nick as nick";
		if ($this->_doauth!==null&&$this->_doauth['flag']<USER_USR)
			$preU = $preU.", T3.fname as full, T3.email as mail";
		// join tables
		$pre2 = " FROM ".USER_LTABLE." T1, ".USER_GTABLE." T2, ".
			USER_UTABLE." T3";
		$pre2 = $pre2." WHERE T1.uid = T3.id AND T1.gid = T2.id";
		$pre1 = "";
		$more = "";
		$step = 0;
		$uid = intval($uid);
		$gid = intval($gid);
		if ($uid>0) {
			$pre1 = $pre1.$preG;
			$more = $more." AND uid=:uid";
			$step++;
		}
		if ($gid>0) {
			$pre1 = $pre1.$preU;
			$more = $more." AND gid=:gid";
			$step++;
		}
		if ($step==2) $pre1 = ", T2.name as gstr, T3.name as name";
		else if (!$step) $pre1 = $preG.$preU;
		$prep = $pre0.$pre1.$pre2.$more;
		try {
			$stmt = $this->prepare($prep);
			if ($uid>0) $stmt->bindValue(':uid',$uid,PDO::PARAM_INT);
			if ($gid>0) $stmt->bindValue(':gid',$gid,PDO::PARAM_INT);
			$stmt->execute();
			$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $err) {
			$this->throw_this("CHKGRPUSR error (".$err->getMessage().")");
		}
		return $list;
	}
	protected function run_count_users() {
		$prep = "SELECT id,flag FROM ".USER_UTABLE;
		$stmt = $this->query($prep);
		$step = 0;
		$result = array();
		$test = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach ($test as $item) {
			$temp = 'lvl'.$item['flag'];
			if (!isset($result[$temp])) $result[$temp] = 1;
			else $result[$temp]++;
			$step++;
		}
		$result['test'] = $test;
		$result['size'] = $step;
		return $result;
	}
	protected function run_list_user($user=null) {
		$prep = "SELECT id, name, flag, nick, fname, email FROM ".USER_UTABLE;
		if ($user!==null) $prep = $prep." WHERE id=".intval($user);
		$stmt = $this->query($prep);
		$step = 0;
		$result = array();
		$result['users'] = [];
		while (1) {
			$item = [];
			$stmt->bindColumn(1,$item['id'],PDO::PARAM_INT);
			$stmt->bindColumn(2,$item['name']);
			$stmt->bindColumn(3,$item['flag'],PDO::PARAM_INT);
			$stmt->bindColumn(4,$item['nick']);
			$stmt->bindColumn(5,$item['fname']);
			$stmt->bindColumn(6,$item['email']);
			if (!$stmt->fetch(PDO::FETCH_BOUND)) break;
			array_push($result['users'],$item);
			$step++;
		}
		$result['size'] = $step;
		return $result;
	}
	protected function run_create_user() {
		$data = $this->_inputs;
		if (empty($data['flag'])||empty($data['name'])||
				empty($data['pass'])||empty($data['email']))
			$this->throw_this("Not enough data!");
		if (empty($data['nick']))
			$data['nick'] = ucfirst(strtolower($data['name'])); // titlecase
		if (empty($data['fname'])) $data['fname'] = $data['nick'];
		// validate flag
		$data['flag'] = $this->check_userflag($data['flag'],USER_NOT);
		// check valid name - only alphanum and underscore
		if (!preg_match("/^[a-zA-Z0-9_]*$/",$data['name']))
			$this->throw_this("Invalid username/userid!");
		// check valid mail
		if (!filter_var($data['email'],FILTER_VALIDATE_EMAIL))
			$this->throw_this("Invalid email!");
		$user = $this->user_insert($data);
		$result['info'] = get_class($this).": User created!";
		$result['user'] = $user;
		return $result;
	}
	protected function run_update_user($user=null) {
		$data = [];
		$data['pass'] = $this->_inputs['pass'];
		$data['nick'] = $this->_inputs['nick'];
		$data['flag'] = $this->_inputs['flag'];
		$data['fname'] = $this->_inputs['fname'];
		$data['email'] = $this->_inputs['email'];
		if ($user!==null) {
			// user cannot change own flag
			if (isset($data['flag'])) $data['flag'] = USER_NOT;
		} else {
			// must be admin
			if (isset($data['flag']))
				$data['flag'] = $this->check_userflag($data['flag'],USER_NOT);
			$user = $this->_inputs['user'];
			if ($user===null)
				$this->throw_this("Invalid id0!");
			$test = $this->user_chk_id($user);
			if ($test===false)
				$this->throw_this("Invalid id1!");
			$name = $this->_inputs['name'];
			if (isset($name)&&$name!==$test['name'])
				$this->throw_this("Invalid id2!");
		}
		if (isset($data['email'])) {
			if (!filter_var($data['email'],FILTER_VALIDATE_EMAIL))
				$this->throw_this("Invalid email!");
		}
		if (empty($data['pass'])&&empty($data['nick'])&&empty($data['flag'])&&
				empty($data['fname'])&&empty($data['email']))
			$this->throw_this("Nothing to update!");
		$this->user_update($user,$data);
		if (isset($user)) $chkmsg = ": Account updated!";
		else $chkmsg = ": User updated!";
		$result['info'] = get_class($this).$chkmsg;
		$result['user'] = $user;
		return $result;
	}
	protected function run_remove_user($user=null) {
		if ($user!==null) {
			if ($this->user_chk_id($user)===false)
				$this->throw_this("Invalid ID!");
		}
		$name = $this->_inputs['name'];
		$mail = $this->_inputs['email'];
		// check user
		$prep = "SELECT id FROM ".USER_UTABLE.
			" WHERE name=:name AND email=:mail";
		try {
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':name',$name,PDO::PARAM_STR);
			$stmt->bindValue(':mail',$mail,PDO::PARAM_STR);
			$stmt->execute();
			$item = $stmt->fetch(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
		} catch (PDOException $err) {
			$this->throw_this("CHKUSR error (".$err->getMessage().")");
		}
		if (!$item) $this->throw_this("No such user!");
		if ($item['id']==1) // protect root
			$this->throw_this("User is invincible!");
		if ($user!==null&&$user!=$item['id'])
			$this->throw_this("Not yours to remove!");
		$exec = $this->user_remove($item['id']);
		if (!$exec) $this->throw_this("Remove failed!");
		if ($user!==null) $chkmsg = "Account removed!";
		else $chkmsg = "User removed!";
		$result['info'] = get_class($this).": ".$chkmsg;
		$result['user'] = $user;
		return $result;
	}
	protected function run_list_group() {
		$prep = "SELECT id, name, full FROM ".USER_GTABLE;
		try {
			$stmt = $this->query($prep);
			$list =  $stmt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $err) {
			$this->throw_this("LISTGRP error (".$err->getMessage().")");
		}
		return $list;
	}
	protected function run_create_group() {
		$name = $this->_inputs['name'];
		$full = $this->_inputs['full'];
		if (empty($name)||empty($full))
			$this->throw_this("Not enough data!");
		// check valid name - only alphanum and underscore
		if (!preg_match("/^[a-zA-Z0-9_]*$/",$name))
			$this->throw_this("Invalid username/userid!");
		$test = $this->group_insert($name,$full,null);
		$result['info'] = get_class($this).": Group created!";
		$result['user'] = $test;
		return $result;
	}
	function run_groups() {
		// methods ignored!
		if ($this->_doauth==null) $auth = USER_NOT;
		else $auth = $this->_doauth['flag'];
		$pick = array_shift($this->_params);
		if (empty($pick)) {
			if (!$this->_valid_&&$auth>=USER_NOT)
				$this->throw_this("Not validated!");
			return $this->run_list_group();
		}
		if ($pick==='create') {
			if ($auth>USER_MOD)
				$this->throw_this("Not privileged!");
			return $this->run_create_group();
		}
		// existing group
		if (!preg_match("/^[a-zA-Z0-9_]*$/",$pick))
			$this->throw_this("Invalid name!");
		$guid = $this->group_find($pick);
		if (!$guid) $this->throw_this("Invalid group!");
		// check group level auth
		$uuid = $this->_doauth===null?0:$this->_doauth['id'];
		$list = $this->group_users_find($uuid,$guid);
		$curr = current($list);
		if ($curr==null) return [];
		// action
		$task = array_shift($this->_params);
		if (empty($task)) {
			if (!$this->_valid_&&$auth>=USER_NOT)
				$this->throw_this("Not validated!");
			$done = $list;
		} else if ($task==="flag") { // modify user level
			$this->throw_this("Flag user!");
		} else if ($task==="pick") { // add user to group
			$this->throw_this("Pick user!");
		} else if ($task==="kick") { // ban user from group
			$this->throw_this("Kick user!");
		} else $this->throw_this("Invalid group task!");
		return $done;
	}
	function run() {
		// look for user path
		$pick = array_shift($this->_params);
		if (empty($pick)) {
			// must be admin to access top level
			if ($this->_doauth['flag']!=USER_ADM)
				$this->throw_this("Not privileged!");
			switch ($this->_method) {
				case 'GET': $result = $this->run_list_user(); break;
				case 'POST': $result = $this->run_create_user(); break;
				case 'PUT': $result = $this->run_update_user(); break;
				case 'DELETE': $result = $this->run_remove_user(); break;
				default: $this->throw_this("Invalid Request!");
			}
		}
		else {
			if ($pick==='groups') // groups management
				return $this->run_groups();
			else if ($pick==='login') { // check if login request
				$result = array();
				if ($this->_doauth!=null) {
					$result = $this->get_current_user();
					// do i need these?
					$result['chkid'] = $this->_doauth['id'];
					$result['uname'] = $this->_doauth['name'];
					$result['unick'] = $this->_doauth['nick'];
					$result['level'] = $this->_doauth['flag'];
					$result['check'] = $this->_method;
					// login info!
					$result['login'] = "okay";
				}
				else $result['login'] = "fail";
				return $result;
			}
			if ($this->_doauth==null)
				$this->throw_this("No user auth!");
			// make sure nothing else is there
			$this->throw_emptypath($this->_params);
			if (!preg_match("/^[a-zA-Z0-9_]*$/",$pick))
				$this->throw_this("Invalid name!");
			// must be id owner
			if ($this->_doauth['name']!=$pick)
				$this->throw_this("Not authorized!");
			$uuid = $this->_doauth['uuid'];
			// process user account request
			switch ($this->_method) {
				case 'GET': $result = $this->run_list_user($uuid); break;
				case 'PUT': $result = $this->run_update_user($uuid);break;
				case 'DELETE': $result = $this->run_remove_user($uuid);break;
				default: $this->throw_this("Invalid Request!");
			}
		}
		return $result;
	}
}
?>
