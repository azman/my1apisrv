<?php
/**
 * Pages.php
 * - task for retrieving web stuffs from resources folder
 * - extends Easy class: auth not enforced
 * - session-based authentication instead
 * - MUST provide trailing / in url (relative path thingy)
**/
require_once dirname(__FILE__).'/../include/Easy.php';
class Pages extends Easy {
	protected $_dopath; // resource path
	function __construct($method,$params,$inputs) {
		parent::__construct($method,$params,$inputs);
		$this->_dopath = dirname(__FILE__)."/../".PAGES_RESPATH;
		// use throw functions AFTER constructor! config-friendly!
		if (!PAGES_ALLOWED)
			$this->throw_this("Pages not allowed!");
		if (!file_exists($this->_dopath)||!is_dir($this->_dopath))
			$this->throw_this("Pages not set up!");
		$this->session_time(intval(PAGES_SESSION_TIMEOUT));
		if (isset($_SESSION[MY1_USER])&&isset($_SESSION[MY1_PASS])) {
			if (!$this->validate($_SESSION[MY1_USER],$_SESSION[MY1_PASS]))
				$this->session_done();
		}
	}
	function run() {
		$size = count($this->_params);
		// must provide trailing / in url
		// - webapp file structure 'compatibility'
		if (!$size) $this->throw_this("Invalid page access!");
		$task = $this->_method;
		$part = implode("/",$this->_params);
		if ($size===1) {
			// a command => serve html or return 
			$pick = PAGES_WORKPHP;
			$file = dirname(__FILE__)."/../".PAGES_FINDPHP."/".$pick.".php";
			if (!file_exists($file))
				throw new Exception("Missing work!");
			include_once $file;
			$work = new $pick($this,$task,$part);
			return $work->work();
		}
		// must be request for resource file
		$full = $this->_dopath."/".$part;
		if (!file_exists($full)||is_dir($full))
			$this->throw_this("Unknown resource! (".$part.")");
		$info = pathinfo($full);
		switch ($info['extension']) {
			case 'css': header('Content-Type: text/css'); break;
			case 'js': header('Content-Type: text/javascript'); break;
			case 'html': // only assign this from work!
				$this->throw_this("Routed! (".$part.")"); break;
			case 'php': // should i 'process' this?
				$this->throw_this("Secured! (".$part.")"); break;
			default: // do not serve 'other' top level file (like .htaccess :o)
				if ($size<2) $this->throw_this("No-sense! (".$part.")");
				break;
		}
		readfile($full);
		exit();
	}
}
?>
