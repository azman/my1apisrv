<?php
/**
 * UsersHelper.php
 * - manage users from comand line
**/
require_once dirname(__FILE__).'/../include/User.php';
require_once dirname(__FILE__).'/../include/TTable.php';
class UsersHelper extends User {
	use TTable {
		ttable_create as public utable_create;
		ttable_delete as public utable_delete;
		ttable_insert as public utable_insert;
		ttable_update as public utable_update;
		ttable_remove as public utable_remove;
		ttable_select as public utable_select;
	}
	protected $_chk0;
	protected $_chk1;
	protected $_chk2;
	function __construct() {
		parent::__construct(true);
		$this->_chk0 = new TTableUser;
		$this->_chk1 = new TTableGroup;
		$this->_chk2 = new TTableGroupUsers;
	}
	function user_get_id($name) {
		return parent::user_get_id($name);
	}
	function pick_user() {
		$this->ttable_copythat($this->_chk0);
	}
	function pick_group() {
		$this->ttable_copythat($this->_chk1);
	}
	function pick_glink() {
		$this->ttable_copythat($this->_chk2);
	}
	function error_quit($mesg) {
		throw new Exception($mesg);
	}
}
try {
	if (PHP_SAPI !== 'cli') { // or php_sapi_name()
		// returns html only if NOT on console?
		header('Content-Type: text/html; charset=utf-8');
		echo "<h1><p>Invalid access!</p></h1>".PHP_EOL;
		exit();
	}
	$test = new UsersHelper();
	// make sure db is ready
	echo "-- Making DB... ";
	$test->do_makedb();
	echo "done.".PHP_EOL;
	echo "-- Opening DB... ";
	$test->do_opendb();
	echo "done.".PHP_EOL;
	$test->pick_user();
	if (!$test->ttable_exists()) {
		echo "-- Creating table ".$test->tname()."... ";
		$test->utable_create();
		echo "done.".PHP_EOL;
	}
	$test->pick_group();
	if (!$test->ttable_exists()) {
		echo "-- Creating table ".$test->tname()."... ";
		$test->utable_create();
		echo "done.".PHP_EOL;
	}
	$test->pick_glink();
	if (!$test->ttable_exists()) {
		echo "-- Creating table ".$test->tname()."... ";
		$test->utable_create();
		echo "done.".PHP_EOL;
	}
	for ($loop=1;$loop<$argc;$loop++) {
		if ($argv[$loop]=='--delete') {
			echo "-- Deleting table ".$test->tname()."... ";
			$test->utable_delete();
			echo "done.".PHP_EOL;
		} else if ($argv[$loop]=='--resetid') {
			echo "-- Resetting id for ".$test->tname()."... ";
			$test->ttable_resetid();
			echo "done.".PHP_EOL;
		} else if ($argv[$loop]=='--select') {
			if ($loop==$argc-1)
				$test->error_quit("No value for ".$argv[$loop]."!");
			$uuid = intval($argv[++$loop]);
			$item = $test->utable_select($test->tname(),null,$uuid);
			echo "-- User:$uuid, Item:".json_encode($item).PHP_EOL;
		} else if ($argv[$loop]=='--list') {
			$test->pick_user();
			echo "-- Listing user in ".$test->tname().":".PHP_EOL;
			$list = $test->utable_select($test->tname());
			foreach ($list as $item)
				echo "## Item:".json_encode($item).PHP_EOL;
		} else if ($argv[$loop]=='--list-group') {
			$test->pick_group();
			echo "-- Listing groups in ".$test->tname().":".PHP_EOL;
			$list = $test->utable_select($test->tname());
			foreach ($list as $item)
				echo "## Item:".json_encode($item).PHP_EOL;
			$test->pick_user(); // alway go back to default
		} else if ($argv[$loop]=='--list-glink') {
			$test->pick_glink();
			echo "-- Listing group users in ".$test->tname().":".PHP_EOL;
			$list = $test->utable_select($test->tname());
			foreach ($list as $item)
				echo "## Item:".json_encode($item).PHP_EOL;
			$test->pick_user(); // alway go back to default
		} else if ($argv[$loop]=='--insert') {
			if ($loop==$argc-1)
				$test->error_quit("No value for ".$argv[$loop]."!");
			$list = explode(',', $argv[++$loop]);
			$cols = [];
			$show = "";
			foreach ($list as $item) {
				$what = explode('=',$item);
				$pkey = array_shift($what);
				$pval = array_shift($what);
				if ($pkey==null||$pval==null||array_shift($what)!==null)
					$test->error_quit("Invalid arg {".$item."}!");
				$cols[$pkey] = $pval;
				$show = $show."[".$pkey."]";
			}
			if (!isset($cols['name']))
				$test->error_quit("Must have userid!");
			if (!isset($cols['pass']))
				$cols['pass'] = $cols['name'];
			$cols['pass'] = hash('sha512',$cols['pass'],false);
			if (!isset($cols['nick']))
				$cols['nick'] = ucfirst(strtolower($cols['name']));
			if (!isset($cols['flag']))
				$cols['flag'] = USER_NOT;
			if (!isset($cols['fname']))
				$cols['fname'] = $cols['nick'];
			if (!isset($cols['email']))
				$cols['email'] = $cols['name']."@localhost";
			echo "-- Insert new user ($show)... ";
			$test->utable_insert($test->tname(),$cols);
			echo "done!".PHP_EOL;
		} else if ($argv[$loop]=='--update') {
			if ($loop==$argc-1)
				$test->error_quit("No value for ".$argv[$loop]."!");
			$list = explode(',', $argv[++$loop]);
			$cols = [];
			$show = "";
			foreach ($list as $item) {
				$what = explode('=',$item);
				$pkey = array_shift($what);
				$pval = array_shift($what);
				if ($pkey==null||$pval==null||array_shift($what)!==null)
					$test->error_quit("Invalid arg {".$item."}!");
				$cols[$pkey] = $pval;
				$show = $show."[".$pkey."]";
			}
			if (!isset($cols['id'])) {
				if (!isset($cols['name']))
					$test->error_quit("Must have id!");
				$uuid = $test->user_get_id($cols['name']);
				if ($uuid===false||$uuid===0)
					$test->error_quit("Invalid user!");
			} else $uuid = $cols['id'];
			echo "-- Update user ($show|$uuid)... ";
			$test->utable_update($test->tname(),$cols,$uuid);
			echo "done!".PHP_EOL;
		} else if ($argv[$loop]=='--remove') {
			if ($loop==$argc-1)
				$test->error_quit("No value for ".$argv[$loop]."!");
			$list = explode(',', $argv[++$loop]);
			$cols = [];
			$show = "";
			foreach ($list as $item) {
				$what = explode('=',$item);
				$pkey = array_shift($what);
				$pval = array_shift($what);
				if ($pkey==null||$pval==null||array_shift($what)!==null)
					$test->error_quit("Invalid arg {".$item."}!");
				$cols[$pkey] = $pval;
				$show = $show."[".$pkey."]";
			}
			if (!isset($cols['id'])) {
				if (!isset($cols['name']))
					$test->error_quit("Must have id!");
				$uuid = $test->user_get_id($cols['name']);
				if ($uuid===false||$uuid===0)
					$test->error_quit("Invalid user!");
			} else $uuid = $cols['id'];
			echo "-- Remove user ($show|$uuid)... ";
			$test->utable_remove($test->tname(),$uuid);
			echo "done!".PHP_EOL;
		} else if ($argv[$loop]=='--insert-group') {
			if ($loop==$argc-1)
				$test->error_quit("No value for ".$argv[$loop]."!");
			$list = explode(',', $argv[++$loop]);
			$cols = [];
			$show = "";
			foreach ($list as $item) {
				$what = explode('=',$item);
				$pkey = array_shift($what);
				$pval = array_shift($what);
				if ($pkey==null||$pval==null||array_shift($what)!==null)
					$test->error_quit("Invalid arg {".$item."}!");
				$cols[$pkey] = $pval;
				$show = $show."[".$pkey."]";
			}
			if (!isset($cols['name']))
				$test->error_quit("Must have groupid!");
			if (!isset($cols['full']))
				$cols['full'] = $cols['name'];
			if (!isset($cols['flag']))
				$cols['flag'] = 0;
			$test->pick_group();
			echo "-- Insert new group ($show)... ";
			$test->utable_insert($test->tname(),$cols);
			echo "done!".PHP_EOL;
			$test->pick_user();
		} else if ($argv[$loop]=='--update-group') {
			if ($loop==$argc-1)
				$test->error_quit("No value for ".$argv[$loop]."!");
			$list = explode(',', $argv[++$loop]);
			$cols = [];
			$show = "";
			foreach ($list as $item) {
				$what = explode('=',$item);
				$pkey = array_shift($what);
				$pval = array_shift($what);
				if ($pkey==null||$pval==null||array_shift($what)!==null)
					$test->error_quit("Invalid arg {".$item."}!");
				$cols[$pkey] = $pval;
				$show = $show."[".$pkey."]";
			}
			if (!isset($cols['id'])) {
				if (!isset($cols['name']))
					$test->error_quit("Must have id!");
				$guid = $test->group_find($cols['name']);
				if ($guid===false||$guid===0)
					$test->error_quit("Invalid group!");
			} else $guid = $cols['id'];
			$test->pick_group();
			echo "-- Update group ($show|$guid)... ";
			$test->utable_update($test->tname(),$cols,$guid);
			echo "done!".PHP_EOL;
			$test->pick_user();
		} else {
			$test->error_quit("Unknown parameter ".$argv[$loop]."!");
		}
	}
	echo "## [".get_class($test)."] Done!".PHP_EOL;
} catch (Throwable $err) { // >php7 using this
	echo PHP_EOL."** Throwable:".$err->getMessage().PHP_EOL.PHP_EOL;
} catch (Exception $err) { // support for older php?
	echo PHP_EOL."** Exception:".$err->getMessage().PHP_EOL.PHP_EOL;
}
?>
