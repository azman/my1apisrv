<?php
/**
 * ThingsHelper.php
 * - create/remove things
**/
require_once dirname(__FILE__).'/../tasks/Things.php';
class ThingsHelper extends Things {
	function __construct() {
		$this->_debug_ = true;
		parent::__construct(null,null,null);
	}
	function thing_create($what,$cols) {
		return parent::thing_create($what,$cols);
	}
	function thing_insert($what,$cols) {
		return parent::thing_insert($what,$cols);
	}
	function thing_delete($what) {
		return parent::thing_delete($what);
	}
	function thing_dolist() {
		return parent::thing_dolist();
	}
}
try {
	if (PHP_SAPI !== 'cli') { // or php_sapi_name()
		// returns html only if NOT on console?
		header('Content-Type: text/html; charset=utf-8');
		echo "<h1><p>Invalid access!</p></h1>".PHP_EOL;
		exit();
	}
	if ($argc<2) exit();
	$test = new ThingsHelper(); // create this early
	for ($loop=1;$loop<$argc;$loop++) {
		if ($argv[$loop]=='--create') {
			if ($loop==$argc-1)
				$test->throw_this("No value for ".$argv[$loop]."!");
			$temp = explode(',', $argv[++$loop]);
			$name = array_shift($temp);
			$cols = [];
			while (1) {
				$what = array_shift($temp);
				if ($what == NULL) break;
				$what = explode('=',$what);
				$pkey = array_shift($what);
				$pval = array_shift($what);
				if ($pkey==NULL||$pval==NULL||array_shift($what)!==NULL) {
					$test->throw_this("Invalid arg {".$what."}!");
				}
				$cols[$pkey] = $pval;
			}
			$test->thing_create($name,$cols);
			echo "-- Thing $name created.".PHP_EOL;
		}
		else if ($argv[$loop]=='--delete') {
			if ($loop==$argc-1)
				$test->throw_this("No value for ".$argv[$loop]."!");
			$name = $argv[++$loop];
			$test->thing_delete($name);
			echo "-- Thing $name deleted.".PHP_EOL;
		}
		else if ($argv[$loop]=='--list') {
			$temp = $test->thing_dolist();
			$size = $temp['size'];
			$list = $temp['list'];
			if ($size) {
				echo "-- LIST:";
				$loop = 0;
				foreach ($list as $what) {
					if (!$loop) echo " ";
					else echo ",";
					echo $what;
					$loop = $loop + 1;
				}
				echo PHP_EOL;
			}
			else echo "-- NoThing in DB.".PHP_EOL;
		}
		else if ($argv[$loop]=='--post') {
			if ($loop==$argc-1)
				$test->throw_this("No value for ".$argv[$loop]."!");
			$temp = explode(',', $argv[++$loop]);
			$name = array_shift($temp);
			$cols = [];
			while (1) {
				$what = array_shift($temp);
				if ($what == NULL) break;
				$what = explode('=',$what);
				$pkey = array_shift($what);
				$pval = array_shift($what);
				if ($pkey==NULL||$pval==NULL||array_shift($what)!==NULL)
					$test->throw_this("Invalid arg {".$what."}!");
				$cols[$pkey] = $pval;
			}
			$test->thing_insert($name,$cols);
			echo "-- Thing $name data posted.".PHP_EOL;
		}
		else throw new Exception("Unknown parameter ".$argv[$loop]."!");
	}
}
catch ( Exception $errmsg ) {
	// nay!
	echo get_class($test)."Error: ".$errmsg->getMessage().PHP_EOL;
}
?>
