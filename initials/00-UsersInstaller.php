<?php
/**
 * UsersInstaller.php
 * - sample installer file to prepare db for Users module
**/
require_once dirname(__FILE__).'/../tasks/Users.php';
class UsersInstaller extends Users
{
	function __construct() {
		$this->_dbskip = true; // in case db in mariadb not created!
		$this->_emesg_ = true;
		parent::__construct(null,null,null);
	}
	protected function group_chk_id($id) {
		$tlbl = USER_GTABLE;
		$prep = "SELECT name FROM $tlbl WHERE id = $id";
		$stmt = $this->query($prep);
		$test = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		return $test;
	}
	function check_default_user() {
		if (!$this->user_chk_id(1)) {
			echo "[adm] ";
			$data = [];
			$data['flag'] = USER_ADM; $data['name'] = 'admin';
			$data['pass'] = hash('sha512',$data['name'],false);
			$data['nick'] = ucfirst(strtolower($data['name']));
			$data['fname'] = 'Administrator';
			$data['email'] = $data['name']."@localhost";
			$this->user_insert($data);
		}
	}
	function check_default_group() {
		if (!$this->group_chk_id(1)) {
			echo "[all] ";
			$data = [];
			$data['name'] = 'public'; $data['full'] = 'Everybody';
			$data['flag'] = 0; $data['user'] = 1;
			$this->group_insert($data); // owned by 'root'!
		}
	}
	function prepdb() {
		if ($this->_dbinit) echo "[init] ";
		echo "[make] ";
		$this->do_makedb();
		echo "[open] ";
		$this->do_opendb();
	}
	function create() {
		echo "[usr+] ";
		$this->user_create();
		echo "[grp+] ";
		$this->group_create();
	}
	function delete() {
		echo "[grp-] ";
		$this->group_delete();
		echo "[usr-] ";
		$this->user_delete();
	}
	function run($remove=false) {
		$this->prepdb();
		// removal request?
		if ($remove) {
			$this->delete();
			return;
		}
		$this->create();
		/* should add 'root' account! */
		$this->check_default_user();
		/* should add default group! - just a 'stub' */
		$this->check_default_group();
	}
}
?>
