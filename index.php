<?php
/**
 * index.php
 * - main handler to look for specific task handler
 * - will always return json data (unless overridden by task)
**/
try {
	$ishere = true;
	define('API_DOBASE',dirname(__FILE__));
	define('API_DOTASK',API_DOBASE.'/tasks');
	require_once API_DOBASE.'/include/config.php';
	if (DEBUG_MODE===true) $doinit = microtime(true);
	$result = array();
	$target = $_SERVER['REQUEST_URI'];
	$method = $_SERVER['REQUEST_METHOD'];
	$inputs = file_get_contents("php://input"); // get raw http content
	// extract params from uri
	$topchk = dirname($_SERVER['SCRIPT_FILENAME']);
	$topchk = explode('/',$topchk);
	$topchk = array_pop($topchk);
	$target = explode('?',$target);
	$params = explode('/',array_shift($target));
	// query strings: ignored!
	//if (!empty($target)) parse_str(array_shift($target),$iquery);
	do {
		$chkchk = array_shift($params); // find api top folder
		if ($chkchk===false) // should NOT happen!
			throw new Exception("Cannot find top level!");
	} while ($chkchk!=$topchk);
	$task = array_shift($params);
	$task = ucfirst(strtolower($task)); // titlecase
	// logger enabled?
	if (LOG_ENABLE) {
		require_once API_DOBASE.'/include/Log.php';
		$keep = new Log(LOG_DONAME);
		$keep->log_request($method,$params,$inputs);
	}
	// find requested task
	$file = API_DOTASK."/$task.php";
	if (!file_exists($file)) {
		$file = API_DOTASK."/$task/$task.php";
		if (!file_exists($file))
			throw new Exception("Missing task!");
	}
	include_once $file;
	//if (!class_exists($task))
	//	throw new Exception("Invalid task!");
	$ishere = false;
	$object = new $task($method,$params,$inputs);
	//if (!method_exists($object,"run"))
	//	throw new Exception("Not runnable task!");
	$dotemp = $object->go_secure($object->run());
	if ($dotemp['secure']===true)
		$result['secure'] = true;
	$result['data'] = $dotemp['godata'];
	$result['flag'] = true;
}
catch (Throwable $errmsg) { // >php7 using this
	if ($ishere&&!DEBUG_MODE&&!EMESG_MODE)
		$emsg = GENERAL_ERRMSG;
	else $emsg = $errmsg->getMessage();
	$result['emsg'] = $emsg;
	$result['flag'] = false;
}
catch (Exception $errmsg) { // support for older php?
	$result['emsg'] = "Exception:".GENERAL_ERRMSG."(".$errmsg->getCode().")";
	$result['flag'] = false;
}
if (DEBUG_MODE) $result['time'] = microtime(true) - $doinit;
if (LOG_ENABLE) $keep->log("{RES}".json_encode($result)."\n");
header('Content-Type: application/json; charset=utf-8');
echo json_encode($result).PHP_EOL;
exit();
?>
