<?php
/**
 * Work.php
 * - base class for handling commands in Pages.php
 * - extends User class
**/
require_once dirname(__FILE__).'/User.php';
class Work extends User {
	protected $_parent;
	protected $_chkcmd;
	protected $_chkusr;
	function __construct($parent,$method,$params) {
		parent::__construct(true); // always skip!
		$this->do_reuse($parent);
		$this->_parent = $parent;
		$this->_chkcmd = $params;
		$this->_chkusr = $user;
	}
	protected function checkParam() {
		$test = [];
		if (isset($_GET['do'])) {
			if ($_GET['do']=='dopass') {
				// page to allow user to change passsword
			} else if ($_GET['do']=='donick') {
			} else if ($_GET['do']=='login') {
			} else {
				// let child classes try?
				$test['get'] = 'do';
			}
		}
		return $test;
	}
	protected function serveHTML($name,$find=null,$code=null) {
		$path = dirname(__FILE__)."/../".PAGES_RESPATH;
		$full = $path."/".$name;
		if (!file_exists($full)||is_dir($full))
			$this->throw_this("Missing resource (".$full.")!");
		header('Content-Type: text/html');
		if ($find!==null&&$code!==null) {
			$temp = fopen($full,'r');
			while (!feof($temp)) {
				$line = rtrim(fgets($temp,4096),"\r\n");
				if (preg_match($find,$line)) echo "$code\n";
				else echo "$line\n";
			}
			fclose($temp);
		} else readfile($full);
		exit();
	}
	function doGET() {
		$this->throw_this("Invalid GET!");
	}
	function doPOST() {
		$this->throw_this("Invalid POST!");
	}
	function doDefaultPage() {
		$code = <<<JSCODE
class MY1Session extends MY1Base {
	constructor(uuid=0,user="guest",nick="Guest") {
		super();
		this.uuid = parseInt(uuid);
		this.user = user;
		this.nick = nick;
	}
}
JSCODE;
		$uuid = 0;
		$user = "guest";
		$nick = "Guest";
		$test = $this->get_current_user();
		if (isset($test['auth'])&&$test['auth']===true) {
			$uuid = $test['uuid'];
			$user = $test['name'];
			$nick = $test['nick'];
		}
		$code = $code."\nvar my1info = ".
				"new MY1Session($uuid,'$user','$nick');";
		$this->serveHTML(PAGES_DEFHTML,"|^//MY1SESSIONJS//$|",$code);
	}
	function work() {
		$test = $this->_chkcmd;
		$args = $this->checkParam();
		if ($test==="login") {
			$done = [];
			$stat = "login";
			$plog = -1;
			$this->session_init();
			if (!isset($_SESSION[MY1_USER])||!isset($_SESSION[MY1_PASS])) {
				if ($this->_doauth!==null) {
					$_SESSION[MY1_USER] = $this->_doauth['name'];
					$_SESSION[MY1_PASS] = $this->_doauth['pass'];
					$done['uuid'] = $this->_doauth['id'];
					$done['user'] = $this->_doauth['name'];
					$done['nick'] = $this->_doauth['nick'];
					$stat = "auth";
					$plog = 0;
				}
				else {
					$stat = "fail";
					$plog = 1;
				}
			}
			else {
				// save _doauth
				$this->_dosave = $this->_doauth;
				if ($this->validate($_SESSION[MY1_USER],$_SESSION[MY1_PASS])) {
					$done['uuid'] = $this->_doauth['id'];
					$done['user'] = $this->_doauth['name'];
					$done['nick'] = $this->_doauth['nick'];
					$stat = "prev";
					$plog = 0;
					// restore _doauth
					$this->_doauth = $this->_dosave;
				}
				else {
					$this->session_done();
					// _doauth not overwritten!
					$stat = "stale";
					$plog = 2;
				}
				$this->_dosave = null;
			}
			$done['stat'] = $stat;
			$done['plog'] = $plog;
			return $done;
		}
		else if ($test==="logout") {
			$this->session_done();
			$done = [];
			$done['stat'] = "done";
			return $done;
		}
		else if ($test==="") {
			$this->doDefaultPage();
		}
		else {
			if ($this->_method==="GET") $this->doGET();
			else if ($this->_method==="POST") $this->doPOST();
			else $this->throw_this("Invalid method!");
		}
		// for debugging??? SHOULD NOT get here!
		$this->throw_this('Invalid option(s):'.json_encode($_GET));
	}
}
?>
