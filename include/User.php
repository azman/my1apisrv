<?php
/**
 * User.php
 * - base class for user data
 * - extends Data class
 * - handles validation
**/
require_once dirname(__FILE__).'/Data.php';
require_once dirname(__FILE__).'/UserData.php';
class User extends Data {
	protected $_doauth;
	function __construct($skip=false) {
		$this->_doauth = null;
		parent::__construct($skip);
	}
	protected function do_reuse($from) {
		if (!is_subclass_of($from,"User"))
			$this->throw_this("Invalid object for do_reuse!");
		$this->_doauth = $from->_doauth;
		return (parent::do_reuse($from)&&$this->_doauth!=null);
	}
	protected function validate($name,$pass) {
		// hashing done by clients - maybe add salt?
		$tlbl = USER_UTABLE;
		$prep = "SELECT id, flag, nick FROM $tlbl WHERE name=? AND pass=?";
		$stmt = $this->prepare($prep);
		try {
			if (!$stmt->bindValue(1,$name,PDO::PARAM_STR)||
					!$stmt->bindValue(2,$pass,PDO::PARAM_STR)) {
				$this->throw_this("Validate bind error!");
			}
			if (!$stmt->execute())
				$this->throw_this("Validate execute error!");
			$item = $stmt->fetch(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
		} catch ( PDOException $error ) {
			$this->throw_this("VALID8 error (".$error->getMessage().")");
		}
		if ($item==false) return false;
		$item['id'] = intval($item['id']);
		$item['name'] = $name;
		$item['pass'] = $pass;
		$this->_doauth = $item;
		return true;
	}
	protected function user_chk_id($id) {
		$tlbl = USER_UTABLE;
		$prep = "SELECT flag, nick, name FROM $tlbl WHERE id=?";
		$stmt = $this->prepare($prep);
		try {
			$stmt->bindValue(1,$id,PDO::PARAM_INT);
			$stmt->execute();
			$item = $stmt->fetch(PDO::FETCH_BOUND);
			$stmt->closeCursor();
		} catch ( PDOException $error ) {
			$this->throw_this("GETUID error (".$error->getMessage().")");
		}
		return $item; // false or array()
	}
	protected function user_get_id($name) {
		$tlbl = USER_UTABLE;
		$prep = "SELECT id FROM $tlbl WHERE name=?";
		$stmt = $this->prepare($prep);
		try {
			$stmt->bindValue(1,$name,PDO::PARAM_STR);
			$stmt->execute();
			//$user = 0;
			$stmt->bindColumn(1,$user,PDO::PARAM_INT);
			$stmt->fetch(PDO::FETCH_BOUND);
			$stmt->closeCursor();
		} catch ( PDOException $error ) {
			$this->throw_this("GETUSR error (".$error->getMessage().")");
		}
		return $user; // zero means user not found!
	}
	function get_current_user() {
		$user = [];
		$user['auth'] = false;
		if ($this->_doauth!=null) {
			$user['uuid'] = $this->_doauth['id'];
			$user['flag'] = $this->_doauth['flag'];
			$user['name'] = $this->_doauth['name'];
			$user['nick'] = $this->_doauth['nick'];
			$user['auth'] = true;
		}
		return $user;
	}
	function session_init() {
		if (session_status()===PHP_SESSION_NONE) {
			session_start();
		}
	}
	function session_done() {
		$this->session_init();
		session_unset();
		session_destroy();
	}
	function session_time($tlen=1800) { // default: 30minutes lifetime
		if ($tlen>0) {
			$this->session_init();
			if (isset($_SESSION['LASTACTIVE'])&&
					(time()-$_SESSION['LASTACTIVE']>$tlen)) {
				session_unset();
				session_destroy();
			}
			$_SESSION['LASTACTIVE'] = time();
		}
	}
}
?>
