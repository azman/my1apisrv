<?php
/**
 * TTable.php
 * - trait for table in Data
 *   = quick way to write interface
 *   = maintains column info in tdata
**/
trait TTable {
	protected $_tname;
	protected $_tdata;
	protected $_tmore;
	function tname() {
		return $this->_tname;
	}
	function tdata() {
		return $this->_tdata;
	}
	function tmore() {
		return $this->_tmore;
	}
	function ttable_clone($tname,$tdata,$tmore) {
		$this->_tname = $tname;
		$this->_tdata = $tdata;
		$this->_tmore = $tmore;
	}
	function ttable_exists($tname=null) {
		if ($tname===null) $tname = $this->_tname;
		return $this->table_exists($tname);
	}
	function ttable_pdotype($dtype) {
		switch ($dtype) {
			case DTYPE_STR: case DTYPE_FLP:
			case DTYPE_STL: case DTYPE_STS:
			case DTYPE_TXT: $type = PDO::PARAM_STR; break;
			case DTYPE_BIG: case DTYPE_INT:
			default: $type = PDO::PARAM_INT; break;
		}
		return $type;
	}
	function ttable_copythat($from) {
		$this->_tname = $from->tname();
		$this->_tdata = $from->tdata();
		$this->_tmore = $from->tmore();
	}
	function ttable_chkarray() {
		$pack = [];
		$pack["name"] = &$this->_tname;
		$pack["data"] = &$this->_tdata;
		$pack["more"] = &$this->_tmore;
		return $pack;
	}
	function ttable_resetid($tname=null) {
		$func = "ttable_resetid";
		if ($tname===null) $tname = $this->_tname;
		if ($this->_dbinit)
			$prep = "ALTER TABLE $tname AUTO_INCREMENT = 1";
		else
			$prep = "UPDATE sqlite_sequence SET seq=1 WHERE name='$tname'";
		try {
			$test = $this->query($prep);
		} catch (PDOException $err) {
			$this->throw_this("$func: {".$err->getMessage()."}");
		}
		return $test;
	}
	protected function ttable_build($cname,$ctype,$cmore=null,$tname=null) {
		if (is_string($tname)&&Data::is_valid_name($tname)) {
			$this->_tname = $tname;
			$this->_tdata = [];
			$this->_tmore = null;
		}
		array_push($this->_tdata,$this->table_column($cname,$ctype,$cmore));
	}
	protected function ttable_build_extra($tmore) {
		if (is_string($tmore)) {
			if ($this->_tmore==null)
				$this->_tmore = [];
			array_push($this->_tmore,$tmore);
		}
	}
	protected function ttable_create($tname=null,$tdata=null,$tmore=null) {
		if ($tname!=null) $this->_tname = $tname;
		if ($tdata!=null) $this->_tdata = $tdata;
		if ($tmore!=null) $this->_tmore = $tmore;
		if (!$this->table_exists($this->_tname))
			$this->table_create($this->_tname,$this->_tdata,$this->_tmore);
	}
	protected function ttable_delete() {
		if ($this->table_exists($this->_tname))
			$this->table_delete($this->_tname);
	}
	protected function ttable_insert($name,$data) {
		$func = "ttable_insert";
		if ($this->_tname!==$name)
			$this->throw_this("$func: Invalid table name!");
		$list = [];
		$pre1 = "INSERT INTO $name (";
		$pre2 = "VALUES (";
		foreach ($this->_tdata as $pick) { // ok for $data to have other stuff!
			$curr = $pick['name'];
			if (array_key_exists($curr,$data)) {
				$pre1 = $pre1.$curr.",";
				$pre2 = $pre2.":".$curr.",";
				$pick['item'] = $data[$curr];
				array_push($list,$pick);
			}
		}
		$pre1 = substr($pre1,0,-1);
		$pre2 = substr($pre2,0,-1);
		$prep = $pre1.") ".$pre2.")";
		try {
			$stmt = $this->prepare($prep);
			foreach ($list as $pick) {
				$type = $this->ttable_pdotype($pick['code']);
				$stmt->bindParam(":".$pick['name'],$pick['item'],$type);
			}
			$exec = $stmt->execute();
			if (!$exec) $this->throw_this("$func: execute error!");
		} catch (PDOException $err) {
			$this->throw_this("$func: {".$err->getMessage()."}");
		}
		return $exec;
	}
	protected function ttable_update($name,$data,$uuid=null) {
		$func = "ttable_update";
		if ($this->_tname!==$name)
			$this->throw_this("$func: Invalid table name!");
		$list = [];
		$prep = "UPDATE $name SET ";
		foreach ($this->_tdata as $pick) {
			$curr = $pick['name'];
			if (array_key_exists($curr,$data)) {
				$prep = $prep."$curr=:$curr, ";
				$pick['item'] = $data[$curr];
				array_push($list,$pick);
			}
		}
		$prep = substr($prep,0,-2);
		if ($uuid!==null) {
			$prep = $prep." WHERE id=:uuid";
			$uuid = intval($uuid);
			if ($uuid==0) $this->throw_this("$func: Invalid id!");
		}
		try {
			$stmt = $this->prepare($prep);
			foreach ($list as $pick) {
				$type = $this->ttable_pdotype($pick['code']);
				$stmt->bindParam(":".$pick['name'],$pick['item'],$type);
			}
			if ($uuid!==null)
				$stmt->bindParam(":uuid",$uuid,PDO::PARAM_INT);
			$exec = $stmt->execute();
			if (!$exec) $this->throw_this("$func: execute error!");
		} catch (PDOException $err) {
			$this->throw_this("$func: {".$err->getMessage()."}");
		}
		return $exec;
	}
	protected function ttable_select($name,$data=null,$uuid=null) {
		$func = "ttable_select";
		if ($this->_tname!==$name)
			$this->throw_this("$func: Invalid table name!");
		$prep = "SELECT ";
		if (isset($data)&&is_array($data)) {
			foreach ($this->_tdata as $pick) {
				$curr = $pick['name'];
				if (in_array($curr,$data))
					$prep = $prep.$curr.",";
			}
			$prep = substr($prep,0,-1);
		} else $prep = $prep."*";
		$prep = $prep." FROM $name";
		if ($uuid!==null) {
			$prep = $prep." WHERE id=:uuid";
			$uuid = intval($uuid);
			if ($uuid==0) $this->throw_this("$func: Invalid id!");
		}
		try {
			$stmt = $this->prepare($prep);
			if ($uuid!==null) $stmt->bindParam(":uuid",$uuid,PDO::PARAM_INT);
			$exec = $stmt->execute();
			if (!$exec) $this->throw_this("$func: execute error!");
			$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $err) {
			$this->throw_this("$func: {".$err->getMessage()."}");
		}
		return $list;
	}
	protected function ttable_remove($name,$uuid=null) {
		$func = "ttable_remove";
		if ($this->_tname!==$name)
			$this->throw_this("$func: Invalid table name!");
		$prep = "DELETE FROM $name";
		if ($uuid!==null) {
			$prep = $prep." WHERE id=:uuid";
			$uuid = intval($uuid);
			if ($uuid==0) $this->throw_this("$func: Invalid id!");
		}
		try {
			$stmt = $this->prepare($prep);
			if ($uuid!==null) $stmt->bindParam(':uuid',$uuid,PDO::PARAM_INT);
			$exec = $stmt->execute();
			if (!$exec) $this->throw_this("$func: execute error!");
		} catch (PDOException $err) {
			$this->throw_this("$func: {".$err->getMessage()."}");
		}
		return $exec;
	}
}
?>
