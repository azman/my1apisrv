<?php
/**
 * Easy.php
 * - task handler for easy access (no auth?) stuffs
 * - extends Task class
 * - example of a quick-and-dirty task handler implementation
 * - can opt for separate sqlite db file
 * - also provide text-file access interface
**/
require_once dirname(__FILE__).'/Task.php';
class Easy extends Task {
	protected $_pfname;
	function __construct($method,$params,$inputs) {
		// by default, validated (no auth required) -> child can reverse this!
		if (!isset($this->_valid_))
			$this->_valid_ = true;
		if (isset($this->_dbfile)) { // do not set if wants to use default db!
			$this->_dbinit = false; // override - cannot use mariadb!
			$path = dirname($this->_dbfile);
			$name = basename($this->_dbfile);
			// force put into DB_SQLITE_PATH
			$this->_dbfile = DB_SQLITE_PATH."/".$name;
			if (!file_exists($this->_dbfile)) {
				$path = DB_SQLITE_PATH;
				$this->_dbfile = $path."/".$name;
				if (!file_exists($path))
					mkdir($path);
				chmod($path,0777);
				touch($this->_dbfile);
				chmod($this->_dbfile,0666);
			}
		}
		parent::__construct($method,$params,$inputs);
		// prepare text data file
		if (!isset($this->_pfname)) $this->file_name("data.txt");
		else $this->file_name(basename($this->_pfname));
		// just try to authenticate, keep on going even if failed to do so!
		$this->authenticate(true);
	}
	// basic file functions
	public function file_name($name) {
		$this->_pfname = DATA_PATH."/".$name;
	}
	public function file_read() {
		if (!file_exists($this->_pfname)) {
			$this->throw_this("File not found!");
		}
		return file_get_contents($this->_pfname);
	}
	public function file_check() {
		if (!file_exists($this->_pfname)) {
			touch($this->_pfname);
			chmod($this->_pfname,0666);
			return false;
		} else {
			return true;
		}
	}
	public function file_append($content) {
		$this->file_check();
		file_put_contents($this->_pfname,$content,LOCK_EX|FILE_APPEND);
	}
	public function file_write($content) {
		$this->file_check();
		file_put_contents($this->_pfname,$content,LOCK_EX);
	}
	public function run() {
		$this->throw_this('Override This!');
	}
}
?>
