<?php
/**
 * Crypt.php
 * - handles encryption/decryption
 * - use base64 encoding
**/
class Crypt {
	protected $_algo; // maybe use aes-256-gcm?
	protected $_hash;
	protected $_size; // iv length
	public function __construct($algo='aes-256-ctr',$hash='sha256') {
		if (!in_array($algo,openssl_get_cipher_methods(true))) {
			$this->throw_this("Invalid algo '$algo'!");
		}
		if (!in_array($hash,openssl_get_md_methods(true))) {
			$this->throw_this("Invalid hash '$hash'!");
		}
		$this->_algo = $algo;
		$this->_hash = $hash;
		$this->_size = openssl_cipher_iv_length($algo);
	}
	protected function throw_this($error) {
		throw new Exception("[".get_class($this)."] ** ".$error);
	}
	public function encrypt($text,$key) {
		$iv = openssl_random_pseudo_bytes($this->_size,$test);
		if (!$test)
			$this->throw_this("Key is not strong enough!");
		$hash = openssl_digest($key,$this->_hash,true);
		$opts = OPENSSL_RAW_DATA;
		$ores = openssl_encrypt($text,$this->_algo,$hash,$opts,$iv);
		if ($ores===false)
			$this->throw_this("Encryption error! (".openssl_error_string().")");
		$temp = $iv . $ores;
		return base64_encode($temp);
	}
	public function decrypt($text,$key) {
		$next = base64_decode($text);
		// and do an integrity check on the size.
		if (strlen($next) < $this->_size)
			$this->throw_this("Length error!");
		$iv = substr($next,0,$this->_size);
		$text = substr($next,$this->_size);
		$hash = openssl_digest($key,$this->_hash,true);
		$opts = OPENSSL_RAW_DATA;
		$ires = openssl_decrypt($text,$this->_algo,$hash,$opts,$iv);
		if ($ires===false)
			$this->throw_this("Decryption error! (".openssl_error_string().")");
		return $ires;
	}
}
?>
