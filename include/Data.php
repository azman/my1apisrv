<?php
/**
 * Data.php
 * - base class for all data
 * - basic database code (open, auth, etc.) using PDO!
**/
require_once dirname(__FILE__).'/config.php';
define('DTYPE_KEY',0x01);
define('DTYPE_INT',0x02);
define('DTYPE_BIG',0x03);
define('DTYPE_FLP',0x04);
define('DTYPE_TXT',0x08); // 0x08 => flag to quote insert data
define('DTYPE_STR',0x09); // @mysql_varchar(64) @sqlite_text
define('DTYPE_STL',0x0A); // long @mysql_varchar(128) @sqlite_text
define('DTYPE_STS',0x0B); // short @mysql_varchar(32) @sqlite_text
class Data {
	protected $_dbmain;
	protected $_dbfile;
	protected $_dbinit;
	protected $_dbname;
	protected $_dbuser;
	protected $_dbpass;
	protected $_debug_;
	protected $_emesg_;
	function __construct($skip=false) {
		$this->_dbmain = null;
		// allows sqlite file name be changed by child
		// - will probably need to change dbinit as well
		if (!isset($this->_dbfile))
			$this->_dbfile = DB_SQLITE_PATH."/".DB_SQLITE_FILE;
		if (!isset($this->_dbinit))
			$this->_dbinit = DB_MARIA_INIT;
		// mariadb settings cannot be changed by child
		$this->_dbname = DB_MARIA_NAME;
		$this->_dbuser = DB_MARIA_USER;
		$this->_dbpass = DB_MARIA_PASS;
		// debugging @ eror messaging mode
		if (!isset($this->_debug_))
			$this->_debug_ = DEBUG_MODE;
		if (!isset($this->_emesg_))
			$this->_emesg_ = EMESG_MODE;
		// do_opendb if not skipped
		if ($skip===false) $this->do_opendb();
	}
	protected function throw_that($error) {
		if (!$this->_debug_ && !$this->_emesg_)
			$error = GENERAL_ERRMSG;
		throw new Exception($error);
	}
	protected function throw_this($error) {
		$this->throw_that(get_class($this).": ".$error);
	}
	protected function do_reuse($from) {
		if (!is_subclass_of($from,"Data"))
			$this->throw_this("Invalid object for do_reuse!");
		// this is the one we need
		$this->_dbmain = $from->_dbmain;
		return ($this->_dbmain!=null);
	}
	function db_ready() {
		return ($this->_dbmain!=null);
	}
	function do_makedb($root="root",$pass="") {
		if ($this->_dbinit) {
			try {
				$db = new PDO("mysql:",$root,$pass);
				$do = "CREATE DATABASE IF NOT EXISTS ".$this->_dbname;
				$db->exec($do);
				$do = "GRANT ALL privileges ON ".$this->_dbname.".*";
				$do = $do." TO '".$this->_dbuser."'@'localhost'";
				$do = $do." IDENTIFIED BY '".$this->_dbpass."';";
				// NOT NEEDED?
				//$do = $do." SET password FOR '".$this->_dbname."'@'%'";
				//$do = $do." = password('".$this->_dbpass."');";
				$do = $do." FLUSH privileges;";
				$db->exec($do);
			}
			catch ( PDOException $error ) {
				$this->throw_this("DB Failure! (".$error->getMessage().")");
			}
		}
		else {
			// create empty file for sqlite
			$dbpath = dirname($this->_dbfile);
			if (!file_exists($dbpath)) {
				mkdir($dbpath);
				chmod($dbpath,0777);
			}
			if (!file_exists($this->_dbfile)) {
				touch($this->_dbfile);
				chmod($this->_dbfile,0666);
			}
		}
	}
	function do_opendb() {
		try {
			if ($this->_dbinit) {
				$this->_dbmain = new PDO("mysql:dbname=".$this->_dbname,
					$this->_dbuser,$this->_dbpass);
			}
			else $this->_dbmain = new PDO("sqlite:".$this->_dbfile);
			$this->_dbmain->setAttribute(PDO::ATTR_ERRMODE,
				PDO::ERRMODE_EXCEPTION);
		}
		catch ( PDOException $error ) {
			$this->throw_this("DB Failure! (".$error->getMessage().")");
		}
	}
	function prepare($query) {
		try { // returns PDOStatement or FALSE
			$result = $this->_dbmain->prepare($query);
		}
		catch ( PDOException $error ) {
			$this->throw_this("Cannot prepare query! (".
				$error->getMessage().")");
		}
		return $result;
	}
	function query($query) {
		try { // returns PDOStatement or FALSE
			$result = $this->_dbmain->query($query);
		}
		catch ( PDOException $error ) {
			$this->throw_this("Cannot execute query! (".
				$error->getMessage().")");
		}
		return $result;
	}
	function table_exists($table) {
		// check if table exists
		if ($this->_dbinit) {
			$prep = "SHOW TABLES LIKE '".$table."'";
		}
		else {
			$prep = "SELECT name FROM sqlite_master WHERE type='table' ".
				"AND name='".$table."'";
		}
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute table_exists!");
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if ($item==false) return false;
		return true;
	}
	function table_create($table,$tdata,$tmore=null) {
		// must exists
		if (empty($table)||empty($tdata)||!is_array($tdata)||
				(isset($tmore)&&!is_array($tmore)))
			$this->throw_this("Not enough data for table_create!");
		// create table
		$prep = "CREATE TABLE IF NOT EXISTS ".$table." ( ";
		foreach ($tdata as $cols) {
			if (!array_key_exists('name',$cols)||
					!array_key_exists('type',$cols))
				$this->throw_this("Invalid format for table_create!");
			$prep = $prep.$cols['name']." ".strtoupper($cols['type']).", ";
		}
		if (isset($tmore)) {
			foreach ($tmore as $xtra) {
				$prep = $prep.$xtra.", ";
			}
		}
		$prep = substr($prep,0,-2);
		$prep = $prep." )";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute table_create!");
		$stmt->closeCursor();
	}
	function table_delete($table) {
		// must exists
		if (empty($table))
			$this->throw_this("Not enough data for table_delete!");
		// delete table
		$prep = "DROP TABLE IF EXISTS ".$table;
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute table_delete!");
		$stmt->closeCursor();
	}
	function view_exists($view) {
		// check if view exists
		if ($this->_dbinit)
			$prep = "SHOW TABLES LIKE '".$view."'";
		else
			$prep = "SELECT name FROM sqlite_master WHERE type='view' ".
				"AND name='".$view."'";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute view_exists!");
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if ($item==false) return false;
		return true;
	}
	function view_create($view,$data,$more) {
		// must exists
		if (empty($view)||empty($data)||!is_array($data)||
				(isset($more)&&!is_array($more)))
			$this->throw_this("Not enough data for view_create!");
		// create view
		if ($this->_dbinit) {
			// temporary? IF NOT EXISTS not supported for mariadb < 10.1.3
			$prep = "CREATE VIEW ".$view." AS SELECT ";
		}
		else $prep = "CREATE VIEW IF NOT EXISTS ".$view." AS SELECT ";
		foreach ($data as $cols) {
			if (!array_key_exists('which',$cols))
				$this->throw_this("Invalid format for view_create!");
			$prep = $prep.$cols['which'];
			if (array_key_exists('alias',$cols))
				$prep = $prep." AS ".$cols['alias'];
			$prep = $prep.", ";
		}
		$prep = substr($prep,0,-2);
		foreach ($more as $xtra) {
			$prep = $prep.$xtra;
		}
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute view_create!");
		$stmt->closeCursor();
	}
	function view_delete($view) {
		// must exists
		if (empty($view))
			$this->throw_this("Not enough data for view_delete!");
		// delete view
		if ($this->_dbinit) $prep = "DROP VIEW ".$view;
		else $prep = "DROP VIEW IF EXISTS ".$view;
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute view_delete!");
		$stmt->closeCursor();
	}
	static function is_valid_name($name) { //validate table/column name
		// name must start with letter, can contain alphanum and underscore
		$patt = "/^[a-zA-Z][a-zA-Z0-9_]*$/";
		$test = preg_match($patt,$name); // returns 1/0/false
		return $test===1?true:false; // check exact matches only!
	}
	static function do_valid_code($code,$init) { //validate column code
		switch ($code) {
			case DTYPE_KEY: $type = "INTEGER PRIMARY KEY";
				if ($init) $type = $type." AUTO_INCREMENT";
				break;
			case DTYPE_STR: $type = $init?"VARCHAR(64)":"TEXT"; break;
			case DTYPE_STL: $type = $init?"VARCHAR(128)":"TEXT"; break;
			case DTYPE_STS: $type = $init?"VARCHAR(32)":"TEXT"; break;
			case DTYPE_FLP: $type = $init?"DOUBLE":"REAL"; break;
			case DTYPE_TXT: $type = "TEXT"; break;
			case DTYPE_BIG: $type = "BIGINT"; break;
			case DTYPE_INT: default: $type = "INTEGER"; break;
		}
		return $type;
	}
	// function to prepare valid column name/type
	function table_column($name,$type,$more=null) {
		// validate name
		if (!self::is_valid_name($name))
			$this->throw_this('Invalid column name?!('.$name.')');
		// validate type
		$code = intval($type);
		$type = self::do_valid_code($code,$this->_dbinit);
		if (is_string($more)) $type = "$type $more";
		return array("name"=>$name,"type"=>$type,"code"=>$code);
	}
	// function to check prepared column name/type: in-place
	function table_column_check(&$tcol) {
		if (empty($tcol)||!is_array($tcol))
			return false;
		if (!isset($tcol['name'])||!isset($tcol['code'])||
				!is_string($tcol['name'])||!is_integer($tcol['code']))
			return false;
		$tcol['type'] = self::do_valid_code($tcol['code'],$this->_dbinit);
		if (isset($tcol['more'])&&is_string($tcol['more']))
			$tcol['type'] = $tcol['type']." ".$tcol['more'];
		return true;
	}
	// function to check prepared table data: in-place
	function table_data_check(&$tdata) {
		foreach ($tdata as &$tcol) {
			if (!$this->table_column_check($tcol))
				return false;
		}
	}
	// function to process a literal table build
	function table_build_check(&$full) {
		// validate table name
		if (empty($full['name'])||!is_string($full['name'])||
				!Data::is_valid_name($full['name']))
			return false;
		// make sure column data is an array
		if (empty($full['data'])||!is_array($full['data']))
			return false;
		// make sure column data array is in valid format
		foreach ($full['data'] as &$tcol) {
			if (!$this->table_column_check($tcol))
				return false;
		}
		//$this->table_data_check($full['data']); // USE THIS! VERIFY FIRST!
		// make sure extra sql is an array IF defined
		if ($full['more']!==null) {
			if (!is_array($full['more'])) return false;
			foreach ($full['more'] as $more)
				if (!is_string($more))
					return false;
		}
		$full['stat'] = "valid";
		return true;
	}
}
?>
