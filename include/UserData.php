<?php
define('USER_UTABLE','my1user');
define('USER_GTABLE','my1group');
define('USER_LTABLE','my1group_users');
define('USER_TYPE_FLAG_MASK',0x0F);
define('USER_ADM',0x00);
define('USER_MOD',0x01);
define('USER_USR',0x02);
define('USER_NOT',0x03);
require_once dirname(__FILE__).'/Data.php';
require_once dirname(__FILE__).'/../include/TTable.php';
class TTableUser extends Data {
	use TTable;
	function __construct() {
		$this->_tname = USER_UTABLE;
		$this->_tdata = array (
			array("name"=>"id","code"=>DTYPE_KEY,"more"=>null),
			array("name"=>"name","code"=>DTYPE_STR,
				"more"=>"UNIQUE NOT NULL"),
			array("name"=>"pass","code"=>DTYPE_STL,"more"=>"NOT NULL"),
			array("name"=>"flag","code"=>DTYPE_INT,"more"=>"NOT NULL"),
			array("name"=>"nick","code"=>DTYPE_STS,"more"=>null),
			array("name"=>"fname","code"=>DTYPE_STL,"more"=>null),
			array("name"=>"email","code"=>DTYPE_STR,
				"more"=>"UNIQUE NOT NULL")
		);
		$this->_tmore = null;
		$pack = $this->ttable_chkarray();
		$this->table_build_check($pack);
	}
}
class TTableGroup extends Data {
	use TTable;
	function __construct() {
		$this->_tname = USER_GTABLE;
		$this->_tdata = array (
			array("name"=>"id","code"=>DTYPE_KEY,"more"=>null),
			array("name"=>"name","code"=>DTYPE_STS,
				"more"=>"UNIQUE NOT NULL"),
			array("name"=>"full","code"=>DTYPE_STL,"more"=>null),
			array("name"=>"flag","code"=>DTYPE_INT,"more"=>"NOT NULL")
		);
		$this->_tmore = null;
		$pack = $this->ttable_chkarray();
		$this->table_build_check($pack);
	}
}
class TTableGroupUsers extends Data {
	use TTable;
	function __construct() {
		$this->_tname = USER_LTABLE;
		$this->_tdata = array (
			array("name"=>"id","code"=>DTYPE_KEY,"more"=>null),
			array("name"=>"uid","code"=>DTYPE_INT,"more"=>null),
			array("name"=>"gid","code"=>DTYPE_INT,"more"=>null),
			array("name"=>"flag","code"=>DTYPE_INT,"more"=>"NOT NULL")
		);
		$this->_tmore = array (
			"UNIQUE (uid,gid)",
			"FOREIGN KEY(uid) REFERENCES ".USER_UTABLE."(id)",
			"FOREIGN KEY(gid) REFERENCES ".USER_GTABLE."(id)"
		);
		$pack = $this->ttable_chkarray();
		$this->table_build_check($pack);
	}
}
?>
