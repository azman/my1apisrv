<?php
function define_if_not_exists($stuff,$value) {
	defined($stuff) or define($stuff,$value);
}
define('MY1CFGMAIN',(__FILE__).'.local');
if (file_exists(MY1CFGMAIN)) include_once MY1CFGMAIN;
// allow downstream to install its own local config
define_if_not_exists('MY1CFGINIT',MY1CFGMAIN);
// general configurations
define_if_not_exists('DATA_PATH',dirname(__FILE__).'/../data');
define_if_not_exists('DB_SQLITE_PATH',DATA_PATH);
define_if_not_exists('DB_SQLITE_FILE','main.sqlite');
define_if_not_exists('DB_MARIA_INIT',false);
define_if_not_exists('DB_MARIA_NAME','kingdb');
define_if_not_exists('DB_MARIA_USER','king');
define_if_not_exists('DB_MARIA_PASS','king');
define_if_not_exists('GENERAL_ERRMSG','Invalid API!');
define_if_not_exists('VALID_DONE',false);
define_if_not_exists('DEBUG_MODE',false);
define_if_not_exists('EMESG_MODE',false);
define_if_not_exists('LOG_ENABLE',false);
define_if_not_exists('LOG_DONAME','main');
define_if_not_exists('API_SECURE',false);
define_if_not_exists('APISRV_MY1KEY','23mj32mj22cd49dj04mk02dg99my26ty');
define_if_not_exists('APISRV_TIMEZONE',"Asia/Kuala_Lumpur");
// run-related configuration
define_if_not_exists('RUN_PATH',"/tmp");
// pages-related configuration
define_if_not_exists('PAGES_ALLOWED',false);
define_if_not_exists('PAGES_RESPATH','resources');
define_if_not_exists('PAGES_DEFHTML','index_demo.html');
define_if_not_exists('PAGES_WORKPHP','Work');
define_if_not_exists('PAGES_FINDPHP','include'); // more php to serve pages
define_if_not_exists('PAGES_SESSION_TIMEOUT',1800); // in seconds
// session-related config (only for pages)
define_if_not_exists('MY1_USER','my1api_username');
define_if_not_exists('MY1_PASS','my1api_userpass');
?>
