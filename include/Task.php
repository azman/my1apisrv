<?php
/**
 * Task.php
 * - parent class for all task handlers
 * - extends User class
 * - copies over parameter from main handler
**/
require_once dirname(__FILE__).'/User.php';
class Task extends User {
	protected $_valid_;
	protected $_debug_;
	protected $_emesg_;
	protected $_apisec;
	protected $_apikey;
	protected $_dbskip;
	protected $_method;
	protected $_params;
	protected $_inputs;
	function __construct($method,$params,$inputs) {
		// can be set by child controllers
		if (!isset($this->_valid_)) $this->_valid_ = VALID_DONE;
		if (!isset($this->_debug_)) $this->_debug_ = DEBUG_MODE;
		if (!isset($this->_emesg_)) $this->_emesg_ = EMESG_MODE;
		if (!isset($this->_apisec)) $this->_apisec = API_SECURE;
		if ($this->_apisec===true) {
			if (!extension_loaded('openssl'))
				$this->throw_this("Encryption not possible!");
		}
		if (!isset($this->_apikey)) $this->_apikey = APISRV_MY1KEY;
		if (!isset($this->_dbskip)) $this->_dbskip = false;
		// pretty much fixed
		$this->_method = $method;
		$this->_params = $params;
		// TODO: use session instead to allow user-specific API key
		if (!empty($inputs)) {
			// check encryption requirement
			if ($this->_apisec===true) {
				// do i need to check content type? maybe enforce json?
				// - for now, expected input is base64-encoded-encrypted-json
				require_once dirname(__FILE__).'/../include/Crypt.php';
				$decode = new Crypt();
				$inputd = $decode->decrypt($inputs,$this->_apikey);
				$inputs = trim($inputd);
			}
			$this->_inputs = json_decode($inputs,true);
		}
		else $this->_inputs = [];
		if ($this->_apisec!==true) {
			// in case url has parameters - only if API_SECURE is NOT true
			foreach ($_GET as $key => $val) {
				if (!array_key_exists($key,$this->_inputs))
					$this->_inputs[$key] = $val;
			}
		}
		// setup User and Data
		parent::__construct($this->_dbskip);
		if ($this->_dbskip===false) // run auth if db is opened
			$this->authenticate($this->_valid_); // if valid, just test auth
	}
	protected function authenticate($test=false) {
		// validate user credentials - need database!
		$username = null; $userpass = null;
		if (isset($this->_inputs['username']))
			$username = $this->_inputs['username'];
		if (isset($this->_inputs['userpass']))
			$userpass = $this->_inputs['userpass'];
		// check user request to help with hashing passwords
		if (isset($this->_inputs['hashpass'])) {
			if ($this->_inputs['hashpass']==="true"&&$userpass!==null) {
				// helps test scripts to hash!
				$userpass = hash('sha512',$userpass,false);
			}
			unset($this->_inputs['userpass']);
			unset($this->_inputs['hashpass']);
		}
		if (empty($username)||empty($userpass)) {
			if ($test===true) return; // allow testing
			$this->throw_this("No credentials!");
		}
		if (!$this->validate($username,$userpass)) {
			if ($test===true) return; // allow testing
			$this->throw_this("Not authorized!");
		}
	}
	protected function throw_emptypath($pathname) {
		if (!empty($pathname)) {
			$this->throw_this("Invalid Path!");
		}
	}
	public function go_secure($output) {
		$result = [];
		if (!$this->_apisec) {
			$result['secure'] = false;
		}
		else {
			if (!empty($output)) {
				$dojson = json_encode($output);
				require_once dirname(__FILE__).'/../include/Crypt.php';
				$decode = new Crypt();
				$output = $decode->encrypt($dojson,$this->_apikey);
			}
			$result['secure'] = true;
		}
		$result['godata'] = $output;
		return $result;
	}
	public function run() {
		throw new Exception("Override run method!");
	}
}
?>
