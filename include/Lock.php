<?php
/**
 * Lock.php
 * - mutex-like object... i hope it to be
 * - from my1vmsapi project
 * - I FORGOT HOW I USED THIS... WILL NEED TO RE-VISIT THIS!
 * - NOT USED AT THE MOMENT?
**/
require_once dirname(__FILE__).'/config.php';
define('LOCKPATH',DATA_PATH);
define('LOCKFILE','DATALOCK');
class Lock {
	protected $_lfname;
	protected $_locked;
	protected $_lftemp;
	protected $_lffile;
	protected $_lfdata;
	function __construct($lockname=LOCKFILE,$lockpath=LOCKPATH) {
		$this->_lfname = $lockpath."/".$lockname;
		$this->_locked = false;
		$this->_lftemp = false; // temp flag, will delete lock file if true
		$this->_lfdata = null;
		if (!file_exists($this->_lfname)) {
			if (!file_exists($lockpath)) {
				mkdir($lockpath);
			}
			chmod($lockpath,0777);
			touch($this->_lfname);
			chmod($this->_lfname,0666);
		}
		$this->_lffile = fopen($this->_lfname,"c+"); //open, write, notrunc
		if ($this->_lffile) {
			// try to lock, non-blocking (will not wait :p)
			if (flock($this->_lffile,LOCK_EX|LOCK_NB)) {
				$this->_lfdata = fgets($this->_lffile);
				$this->_locked = true;
			} else {
				fclose($this->_lffile);
				$this->_lffile = null;
			}
		}
	}
	function __destruct() {
		if ($this->_lffile) {
			if ($this->_locked===true) {
				flock($this->_lffile,LOCK_UN);
			}
			fclose($this->_lffile);
		}
		if ($this->_lftemp===true&&file_exists($this->_lfname)) {
			unlink($this->_lfname);
		}
	}
	function isLocked() {
		return $this->_locked;
	}
	function getData() {
/*
		if ($this->_locked===true) {
			rewind($this->_lffile);
			//fseek($this->_lffile,0,SEEK_SET);
			$this->_lfdata = fgets($this->_lffile);
		}
*/
		return $this->_lfdata;
	}
	function putData($data) {
		$this->_lfdata = $data;
		if ($this->_locked===true) {
			rewind($this->_lffile);
			//fseek($this->_lffile,0,SEEK_SET);
			fputs($this->_lffile,$this->_lfdata,strlen($this->_lfdata));
		}
	}
	function timeStamp() {
		$zone = new DateTimeZone("Asia/Kuala_Lumpur");
		$here = new DateTime("now",$zone);
		$this->putData($here->format('YmdHis'));
	}
	function tempLock($istemp=true) {
		$this->_lftemp = $istemp;
	}
}
/*
try {
	if (PHP_SAPI !== 'cli') exit();
	$hang = false;
	for ($loop=1;$loop<$argc;$loop++) {
		if ($argv[$loop]==='--hang') {
			$hang = true;
		} else {
			throw new Exception("Unknown parameter ".$argv[$loop]."!");
		}
	}
	$data = new Lock("test.txt",dirname(__FILE__));
	$data->tempLock();
	if (!$data->isLocked())
		throw new Exception("Cannot get lock!");
	echo "Check: {".$data->getData()."}".PHP_EOL;
	$data->timeStamp();
	echo "Write: {".$data->getData()."}".PHP_EOL;
	if ($hang===true) {
		$test = fopen("php://stdin","r");
		// assume ok
		while(strtolower(fgetc($test))!=='q');
		fclose($test);
	}
	echo "[YAY] Completed!".PHP_EOL;
} catch ( Exception $errmsg ) {
	echo "[NAY] ".$errmsg->getMessage().PHP_EOL;
}
*/
?>
