<?php
/**
 * Log.php
 * - simple class for text log implementation
**/
require_once dirname(__FILE__).'/config.php';
class Log {
	protected $_logger;
	protected $_tzone_; // datetimezone object
	function __construct($name) {
		$this->_logger = DATA_PATH."/".$name.".log";
		if (!file_exists($this->_logger)) {
			touch($this->_logger);
			chmod($this->_logger,0666);
		}
		$this->_tzone_ = new DateTimeZone(APISRV_TIMEZONE);
	}
	public function log_request($method,$params,$inputs,$secure=API_SECURE) {
		$dlog = [ "from"=>$_SERVER['REMOTE_ADDR'], "method"=>$method,
			"params"=>$params ];
		if (!empty($inputs)) {
			if ($secure===true) // expecting base64 string
				$dlog["inputs"] = $inputs;
			else // expecting json text
				$dlog["inputs"] = json_decode($inputs,true);
		}
		$this->log("{REQ}".json_encode($dlog)."\n");
	}
	public function log($text) {
		$mark = new DateTime("now",$this->_tzone_);
		$full = "[".$mark->format("YmdHis")."] ".$text;
		file_put_contents($this->_logger,$full,LOCK_EX|FILE_APPEND);
	}
}
?>
