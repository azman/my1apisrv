<?php
/**
 * install.php
 * - one installer to rule them all
**/
ini_set('error_reporting',E_ALL);
require_once dirname(__FILE__).'/include/config.php';
function delete_all($name) {
	$icnt = 0;
	if (!file_exists($name)) return 0;
	if (is_dir($name)) {
		$list = glob($name.'/*');
		foreach ($list as $file) {
			$icnt += delete_all($file);
		}
		echo "-- Removing path ".$name."... ";
		if (rmdir($name)===true) echo "done.";
		else  echo "**error**";
		echo PHP_EOL;
	}
	else if (is_file($name)) {
		echo "-- Removing file ".$name."... ";
		if (unlink($name)===true) echo "done.";
		else  echo "**error**";
		echo PHP_EOL;
	}
	else echo "?? Check:".$name.PHP_EOL;
	return 1;
}
function find_installer($name) {
	$find = dirname(__FILE__).'/initials/'.$name.'Installer.php';
	if (file_exists($find)) return $find;
	$list = glob("tasks/*/".$name."Installer.php",GLOB_BRACE);
	if (!empty($list)) {
		$find = array_shift($list);
		if (file_exists($find)) return $find;
	}
	return null;
}
function run_single_installer($do_file,$do_del=false) {
	$do_test = basename($do_file,".php");
	include_once $do_file;
	$do_call = explode('-',$do_test);
	if (count($do_call)>1)
		array_shift($do_call); // skip the numeric index
	$do_inst = array_shift($do_call);
	echo "-- Running ".$do_inst." (".($do_del?"-":"+").")... ";
	$do_call = new $do_inst();
	$do_call->run($do_del);
	echo "done.".PHP_EOL;
}
define('ITASK_NONE',0);
define('ITASK_INSTALL',0x01);
define('ITASK_REMOVE',0x02);
define('ITASK_WIPE',0x04);
try {
	if (PHP_SAPI !== 'cli') { // or php_sapi_name()
		// returns html only if NOT on console?
		header('Content-Type: text/html; charset=utf-8');
		echo "<h1><p>Invalid access!</p></h1>".PHP_EOL;
		exit();
	}
	// options
	$task = ITASK_INSTALL; // by default, do install
	$irem = false;
	for ($loop=1;$loop<$argc;$loop++) {
		if ($argv[$loop]==='--config') $task = ITASK_NONE;
		else if ($argv[$loop]==='--remove') $task = ITASK_REMOVE;
		else if ($argv[$loop]==='--wipe') $task = ITASK_REMOVE|ITASK_WIPE;
		else if ($argv[$loop]==='--single') {
			if (++$loop>=$argc) throw new Exception("** No value for option!");
			$do_file = $argv[$loop];
			if (!file_exists($do_file)) {
				$do_temp = find_installer($do_file);
				if ($do_temp===null||!file_exists($do_temp))
					throw new Exception("** File not found! (".$do_file.")");
				$do_file = $do_temp;
			}
			run_single_installer($do_file,$task&ITASK_REMOVE?true:false);
			exit();
		}
		else throw new Exception("** Unknown option (".$argv[$loop].")!");
	}
	if ($task&ITASK_REMOVE) {
		$task &= ~ITASK_INSTALL;
		$irem = true;
	}
	// do that...
	if ($task!==ITASK_NONE) {
		// loop through all installers
		$stuffs = glob("{initials/*.php,tasks/*/*Installer.php}",GLOB_BRACE);
		if ($irem) $stuffs = array_reverse($stuffs);
		foreach ($stuffs as $stuff)
			run_single_installer($stuff,$irem);
	}
	if (!$irem) { // only if we are not removing
		// make sure that exists
		if (file_exists(MY1CFGINIT)) {
			echo "## Reading ".MY1CFGINIT.PHP_EOL;
			$text = file_get_contents(MY1CFGINIT);
		}
		else {
			echo "## Creating new ".MY1CFGINIT.PHP_EOL;
			$text = "<?php\n?>\n";
		}
		$list = glob("include/config*.php",GLOB_BRACE);
		$find = "/^define_if_not_exists/";
		$tell = 0;
		foreach ($list as $conf) {
			$curr = 0;
			$temp = fopen($conf,'r');
			while (!feof($temp)) {
				$line = rtrim(fgets($temp,4096),"\r\n");
				if (preg_match($find,$line)) {
					$next = preg_replace($find,"//define",$line);
					//echo "-- Found:{".$next."}".PHP_EOL;
					$chk1 = strpos($next,"'");
					if ($chk1===false) continue; // assume invalid
					$chk1++;
					$chk2 = strpos($next,"'",$chk1);
					if ($chk2===false) continue;
					$tvar = substr($next,$chk1,$chk2-$chk1);
					//echo "-- Found:{".$tvar."}(".$chk1.")(".$chk2.")".PHP_EOL;
					$test = preg_match("|define\('".$tvar."',|",$text);
					if ($test===0) {
						if ($curr===0) {
							$curr++;
							echo "@@ From:{".basename($conf)."}".PHP_EOL;
						}
						echo "## Adding config '$tvar'".PHP_EOL;
						$text = preg_replace("/^(\?>)$/m","$next\n$1",$text);
						$tell = 1;
					}
				}
			}
			fclose($temp);
		}
		if ($tell===1) {
			echo "-- Updating ".MY1CFGINIT.PHP_EOL;
			file_put_contents(MY1CFGINIT,$text);
		}
	}
	if ($task&ITASK_WIPE) {
		$do_data = dirname(__FILE__).'/data';
		delete_all($do_data);
		if (file_exists(MY1CFGINIT)) {
			if (!unlink(MY1CFGINIT))
				throw new Exception("Error removing local config file!");
			echo "-- Local config removed! (".
				basename(MY1CFGINIT).")".PHP_EOL;
		}
	}
} catch (Exception $error) {
	echo PHP_EOL.PHP_EOL;
	echo "** Installation error! [".$error->getMessage()."]".PHP_EOL.PHP_EOL;
}
exit();
?>
